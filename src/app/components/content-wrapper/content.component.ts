import { Component } from '@angular/core';
import { mainConfig } from '../../config'

@Component({
  selector: 'bit-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {
  private menuItems
  private title

  constructor() {
    this.menuItems = []
    this.title = mainConfig.title
  }

  getImgSliderContent() {
    const images = [
      {
        url: '/assets/header-background.png',
        title: 'Changing the future'
      },
      {
        url: '/assets/gif-test.gif',
        title: 'Testing a second slider'
      }
    ]
    return images
  }
}