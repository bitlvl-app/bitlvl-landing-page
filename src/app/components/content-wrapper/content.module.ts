import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ContentComponent } from './content.component';
import { SliderComponent } from '../slider/slider.component'
import { PresentationComponent } from '../presentation/presentation.component'
import { FooterComponent } from '../footer/footer.component'

@NgModule({
  declarations: [
    ContentComponent,
    SliderComponent,
    PresentationComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  exports: [ContentComponent]
})
export class ContentModule { }