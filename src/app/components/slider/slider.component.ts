import { Component, Input } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'image-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent {
  @Input() contents;
  @Input() config?;

  constructor() { }

  ngAfterContentInit() {
    this.beginSlider();
  }

  beginSlider() {
    const callSlider = this.sliderResponsive
    const config = this.config
    $(document).ready(function () {
      callSlider({ config }, '#slider1');
    });
  }

  sliderResponsive(settings, sliderId) {
    var set = $.extend(
      {
        slidePause: 8000,
        fadeSpeed: 800,
        autoPlay: true,
        showArrows: true,
        hideDots: true,
        hoverZoom: true,
        titleBarTop: false
      },
      settings
    );

    var $slider = $(sliderId);
    var size = $slider.find("> div").length; //number of slides
    var position = 0; // current position of carousal
    var sliderIntervalID; // used to clear autoplay

    // Add a Dot for each slide
    $slider.append("<ul></ul>");
    $slider.find("> div").each(function () {
      $slider.find("> ul").append('<li></li>');
    });

    // Put .show on the first Slide
    $slider.find("div:first-of-type").addClass("show");

    // Put .showLi on the first dot
    $slider.find("li:first-of-type").addClass("showli")

    //fadeout all items except .show
    $slider.find("> div").not(".show").fadeOut();

    // If Autoplay is set to 'on' than start it
    if (set.autoPlay) {
      startSlider();
    }

    // If showarrows is set to 'on' then don't hide them
    if (set.showArrows) {
      $slider.addClass('showArrows');
    }

    // If hideDots is set to 'on' then hide them
    if (set.hideDots) {
      $slider.addClass('hideDots');
    }

    // If hoverZoom is set to 'off' then stop it
    if (set.hoverZoom) {
      $slider.addClass('hoverZoomOff');
    }

    // If titleBarTop is set to 'on' then move it up
    if (set.titleBarTop) {
      $slider.addClass('titleBarTop');
    }

    // function to start auto play
    function startSlider() {
      sliderIntervalID = setInterval(function () {
        nextSlide();
      }, set.slidePause);
    }

    // on mouseover stop the autoplay
    $slider.mouseover(function () {
      if (set.autoPlay) {
        clearInterval(sliderIntervalID);
      }
    });

    // on mouseout starts the autoplay
    $slider.mouseout(function () {
      if (set.autoPlay) {
        startSlider();
      }
    });

    //on right arrow click
    $slider.find("> .right").click(nextSlide)

    //on left arrow click
    $slider.find("> .left").click(prevSlide);

    // Go to next slide
    function nextSlide() {
      position = $slider.find(".show").index() + 1;
      if (position > size - 1) position = 0;
      $slider.find(".show").removeClass("show").fadeOut();
      $slider
        .find("> div")
        .eq(position)
        .fadeIn(set.fadeSpeed)
        .addClass("show");
      // The Dots
      $slider.find("> ul").find(".showli").removeClass("showli");
      $slider.find("> ul > li").eq(position).addClass("showli");
    }

    // Go to previous slide
    function prevSlide() {
      position = $slider.find(".show").index() - 1;
      if (position < 0) position = size - 1;
      $slider.find(".show").removeClass("show").fadeOut();
      $slider
        .find("> div")
        .eq(position)
        .fadeIn(set.fadeSpeed)
        .addClass("show");
      // The Dots
      $slider.find("> ul").find(".showli").removeClass("showli");
      $slider.find("> ul > li").eq(position).addClass("showli");
    }

    //when user clicks slider button
    $slider.find(" > ul > li").click(function () {
      position = $(this).index();
      $slider.find(".show").removeClass("show").fadeOut();
      $slider
        .find("> div")
        .eq(position)
        .fadeIn(set.fadeSpeed)
        .addClass("show");
      // The Dots
      $slider.find("> ul").find(".showli").removeClass("showli");
      $slider.find("> ul > li").eq(position).addClass("showli");
    });

    //this changes the image and button selection
    return $slider;
  };

}
